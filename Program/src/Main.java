package tp3;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

public class Main 
{
	
	public static void main(String[] args) 
	{
		int NUM_PLACES = 16;
		int NUM_TRANSITIONS = 15;
		int NUM_DATA = 1000;
		
		int LOG_DELAY = 100;

		// Variables for time calculation
		float finalTime;
		long totalTime;
		long startTime =  System.currentTimeMillis();
		
		String LOG_FILE = "log.txt";
		String TINV_LOG = "tinvariants.txt";
		
		ExcelFiles fileHandler = new ExcelFiles("PetriNet.xls", NUM_PLACES, NUM_TRANSITIONS);

		PetriNet RdP = new PetriNet(fileHandler, NUM_DATA);
		
		Policy p = new Policy(RdP, NUM_TRANSITIONS);
		
		Monitor m = new Monitor(RdP, p, NUM_TRANSITIONS);
		
		Thread.currentThread().setName("MAIN");
		
		System.out.printf("Main creates threads\n");
		
		CPUPower cpower[] = new CPUPower[2];
		cpower[0] = new CPUPower(0, m, RdP);
		cpower[1] = new CPUPower(1, m, RdP);
		
		CPUTask ctask[] = new CPUTask[2];
		ctask[0] = new CPUTask(0, m, RdP);
		ctask[1] = new CPUTask(1, m, RdP);
		
		CPUStay_ON cpuon[] = new CPUStay_ON[2];
		cpuon[0] = new CPUStay_ON(0, m, RdP);
		cpuon[1] = new CPUStay_ON(1, m, RdP);
		
		ArrivalData a[] = new ArrivalData[2];
		a[0] = new ArrivalData(0, RdP, m);
		a[1] = new ArrivalData(1, RdP, m);
		
		GeneradorTareas g = new GeneradorTareas(m, NUM_DATA);
		
		// Elements for the LOG
		FileWriter fileWriter = null;
		PrintWriter printWriter = null;
		try
		{
			fileWriter = new FileWriter(LOG_FILE);
			printWriter = new PrintWriter(fileWriter);
		} 
		catch (Exception e)
		{
			e.printStackTrace();
		}
		
		// T-Invariants log
		FileWriter fileWriter2 = null;
		PrintWriter printWriter2 = null;
		try
		{
			fileWriter2 = new FileWriter(TINV_LOG);
			printWriter2 = new PrintWriter(fileWriter2);
		} 
		catch (Exception e)
		{
			e.printStackTrace();
		}
		
		Log log = new Log(printWriter , printWriter2 ,RdP ,LOG_DELAY);
		
		// Start Threads
		System.out.print("MAIN: Threads start\n");

		for(int i = 0 ; i < 2 ; i++)
		{
			cpower[i].setName("CP" + (i +1));
			cpower[i].start();
			cpuon[i].setName("CPON" + (i +1));  // CPUStay_ON Class
			cpuon[i].start();
			a[i].setName("AD" + (i+1));
			a[i].start();
			ctask[i].setName("CC" + (i+1));
			ctask[i].start();
		}

		g.setName("GEN");
		g.start();
		
		log.start();
		
		try {g.join();} catch (InterruptedException e) {e.printStackTrace();}
		
		// Wait for the remaining threads to finish (or equivalently, for the tasks to finish processing)
		try 
		{
			for(int i = 0 ; i < 2 ; i++)
			{
				cpower[i].join();
				ctask[i].join();
				a[i].join();
			}
		}catch (InterruptedException e) {e.printStackTrace();}
		
		totalTime = System.currentTimeMillis() - startTime;
		finalTime = totalTime/ 1000F;
		
		System.out.printf("MAIN: ");
		RdP.printMarking();
		
		log.end(finalTime);

		// Wait until the log finishes printing
		try{
			log.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		// Close the corresponding files
		try{
			if(null != fileWriter && null != fileWriter2)
				fileWriter.close();
				fileWriter2.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		System.out.println("MAIN: Total time: "+finalTime);
		System.out.print("MAIN: End of execution\n");
	}

}