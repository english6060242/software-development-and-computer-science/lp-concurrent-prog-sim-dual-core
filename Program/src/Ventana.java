package tp3;

public class Ventana 
{
	private long alpha;
	private long beta;

	public Ventana()
	{
		this.alpha = 0;
		this.beta = 0;
	}
	
	public void setAlpha (long a)
	{
		this.alpha = a;
	}
	
	public void setBeta (long b)
	{
		this.beta = b;
	}
	
	public long getAlpha()
	{
		return this.alpha;
	}
	
	public long getBeta()
	{
		return this.beta;
	}
	
}
