package tp3;

public class CPUStay_ON extends Thread 
{
	private int id;
	private Monitor m;
	private PetriNet Rdp;
	
	public CPUStay_ON(int id, Monitor m, PetriNet Rdp)
	{
		this.id = id;
		this.m = m;
		this.Rdp = Rdp;
	}
	
	public void run()
	{
		while(!this.Rdp.getTerminated())
		{
			if(!this.Rdp.getTerminated())
			{
				if(this.id == 0)
				{
					try 
		        	{
						this.m.enterAndFire(6);
		            } 
		        	catch (InterruptedException e) 
		        	{
		        		System.out.printf("GENERATOR FAILURE\n");
		                return;
		        	}
				}
				if(this.id == 1)
				{
					try 
		        	{
						this.m.enterAndFire(13);
		            } 
		        	catch (InterruptedException e) 
		        	{
		        		System.out.printf("GENERATOR FAILURE\n");
		                return;
		        	}
				}
			}
		}
	}
}