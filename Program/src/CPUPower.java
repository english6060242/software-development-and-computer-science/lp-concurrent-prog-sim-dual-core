package tp3;

public class CPUPower extends Thread 
{	
	private int id;
	private Monitor m;
	private PetriNet Rdp;
	
	public CPUPower(int id, Monitor m, PetriNet Rdp)
	{
		this.id = id;
		this.m = m;
		this.Rdp = Rdp;
	}
	
	public void run()
	{
		while(!this.Rdp.getTerminated())
		{
			if(!this.Rdp.getTerminated())
			{
				if(this.id == 0)
				{
					try 
		        	{
						this.m.enterAndFire(7);
						this.m.enterAndFire(5);						
						this.m.enterAndFire(4);
		            } 
		        	catch (InterruptedException e) 
		        	{
		        		System.out.printf("GENERATOR FAILURE\n");
		                return;
		        	}
				}
				if(this.id == 1)
				{
					try 
		        	{
						this.m.enterAndFire(14);
						this.m.enterAndFire(12);
						this.m.enterAndFire(11);
		            } 
		        	catch (InterruptedException e) 
		        	{
		        		System.out.printf("GENERATOR FAILURE\n");
		                return;
		        	}
				}
			}
		}
	}
}
