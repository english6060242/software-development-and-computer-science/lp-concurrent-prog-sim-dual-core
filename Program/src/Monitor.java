package tp3;

import java.util.LinkedList;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

public class Monitor 
{
	private ReentrantLock lock = new ReentrantLock(true);
	private LinkedList<Condition> waitStack = new LinkedList<>();
	private int transitions;
	private PetriNet Rdp;
	private Policy p;
	private boolean asleepThreads[];
	private boolean isSleepingAlpha[];
	
	public Monitor(PetriNet Rdp, Policy p, int t)
	{
		this.Rdp = Rdp;
		this.p = p;
		this.transitions = t;
		for(int i = 0; i< this.transitions; i++)	// Create as many condition queues as transitions.
		{
			waitStack.add(lock.newCondition());
		}
		asleepThreads = new boolean[this.transitions];
		for (int i = 0; i< transitions ; i++)
		{
			asleepThreads[i] = false;
		}
		isSleepingAlpha = new boolean[this.transitions];
		for (int i = 0; i< transitions ; i++)
		{
			isSleepingAlpha[i] = false;
		}
	}
	
	public void entroydisparo(int transition) throws InterruptedException
	{
			int awake;
			long alpha;
			long now;
			long timestamp;
			long sleepTime;
			try
			{
				lock.lock();
				while(!Rdp.isSensitized(transition))
				{
					if(this.Rdp.isTerminated())
					{
						break;
					}
					asleepThreads[transition] = true;
					waitStack.get(transition).await();
				}
				if(!this.Rdp.isTerminated())
				{
					if(Rdp.isTimed(transition))
					{
						alpha = Rdp.getWindow(transition).getAlpha();
						timestamp = Rdp.getTimeStamp(transition);
						now = System.currentTimeMillis();
						if(alpha > (now - timestamp))
						{
							sleepTime = alpha - (now - timestamp);
							isSleepingAlpha[transition] = true;
							lock.unlock();
							Thread.sleep(sleepTime);
							lock.lock();
							isSleepingAlpha[transition] = false;
						}
					}
					asleepThreads[transition] = false;
					Rdp.fire(transition);
					Rdp.printMarking();
					System.out.print("Processed CPU1:  "+Rdp.getProcessedB1()+" Processed CPU2:  "+Rdp.getProcessedB2()+"\n");
					awake = p.whichThreadToWake(asleepThreads,isSleepingAlpha);  // Ask the policy which thread to wake up
					waitStack.get(awake).signal();
				}
				if(this.Rdp.isTerminated())
				{
					for(int i = 0; i < this.transitions; i++)
					{
						if(asleepThreads[i])
						{
							waitStack.get(i).signal();  
						}
					}
				}
			}
			finally 
			{
				// After meeting the firing condition, the transition is fired and the code sequence returns
				// while still inside the monitor (lock.lock).
				lock.unlock();
			}
	}

}