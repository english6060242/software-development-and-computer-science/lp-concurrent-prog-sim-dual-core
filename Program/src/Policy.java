package tp3;

public class Policy {
	private PetriNet Rdp;
	private int transitions;
	private boolean flag;
	private boolean[] T_awake;
    
	public Policy(PetriNet Rdp, int t)	// caution, only one thread should be awakened, only one transition
	{
		this.transitions = t;
		this.Rdp = Rdp;
		this.flag = false;
		T_awake = new boolean[this.transitions];
	}
	   
	public void fireableTransitions()
	{
		for(int i = 0; i < transitions; i++)
		{
			if(this.Rdp.isSensibilizada(i))
			{
				T_awake[i] = true;
			}
			else
			{
				T_awake[i] = false;
			}
		}
		if(this.Rdp.isSensibilizada(1) && this.Rdp.isSensibilizada(8))
		{
			flag = !flag;
			if(flag)
			{
				T_awake[8] = false;	// Only awaken the thread firing T1
				if(this.Rdp.getMarcas(2) > this.Rdp.getMarcas(9))
				{
					// Only awaken the thread firing T8
					T_awake[1] = false;
					T_awake[8] = true;	
				}
			}
			else
			{
				T_awake[1] = false;  // Only awaken the thread firing T8
				if(this.Rdp.getMarcas(2) < this.Rdp.getMarcas(9))
				{
					// Only awaken the thread firing T1
					T_awake[1] = true;
					T_awake[8] = false;	
				}
			}
		}
	}
	    
	public int toFire(boolean asleepThreads[], boolean isSleepingAlpha[])
	{
		fireableTransitions();
		for (int i = 0; i < transitions; i++)	// The first asleep thread corresponding to a fireable transition will be awakened
		{										// Fireable: Tries to fire a sensitized transition, chosen by the policy in case of conflict 
			if (T_awake[i] && asleepThreads[i] && (!isSleepingAlpha[i])) // and is not asleep due to timing issues
			{
				return i;
			}
		}
		return 0;
	}
}
