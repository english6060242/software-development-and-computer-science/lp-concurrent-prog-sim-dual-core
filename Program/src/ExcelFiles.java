package tp3;

import java.io.File;
import java.io.FileInputStream;
import java.util.Iterator;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;

public class ExcelFiles {

	String fileName;
	int nPlaces;
	int nTransitions;
	
	public ExcelFiles(String name, int p, int t)
	{
		this.fileName = name;
		this.nPlaces = p;
		this.nTransitions = t;
	}
	
	public int[] getInitialMarking()
	{
		int[] marking = new int[this.nPlaces];
		try(FileInputStream file = new FileInputStream(new File(this.fileName)))
		{	
			// Create HSSFWorkbook object to handle Excel XLS
			HSSFWorkbook workbook = new HSSFWorkbook(file);
			
			// Get the third sheet
			HSSFSheet sheet = workbook.getSheetAt(2);
			
			// Iterate row by row and cell by cell
			Iterator<Row> rowIt = sheet.iterator();
			int i = 0;
			while(rowIt.hasNext())
			{
				Row row = rowIt.next();
				Iterator<Cell> cellIterator = row.cellIterator();
				Cell cell = cellIterator.next();
				marking[i] = (int)cell.getNumericCellValue();
				i++;
			}
			workbook.close();
			file.close();
		}catch (Exception e){}
		return marking;
	}
	
	public int[][] getMatrix(int p)
	{
		int[][] matrix = new int[this.nPlaces][this.nTransitions];
		try(FileInputStream file = new FileInputStream(new File(this.fileName)))
		{	
			// Create HSSFWorkbook object to handle Excel XLS
			HSSFWorkbook workbook = new HSSFWorkbook(file);
			
			// Get the first sheet
			HSSFSheet sheet = workbook.getSheetAt(p);
			
			// Iterate row by row and cell by cell
			Iterator<Row> rowIt = sheet.iterator();
			Row row;
			int i = 0;
			while(rowIt.hasNext())
			{
				int j = 0;
				row = rowIt.next();
				Iterator<Cell> cellIterator = row.cellIterator();
				Cell cell;
				while(cellIterator.hasNext())
				{
					cell = cellIterator.next();
					matrix[i][j] = (int)cell.getNumericCellValue();
					if(j < this.nTransitions)
						j++;
				}
				if(i < this.nPlaces)
					i++;
			}
			workbook.close();
			file.close();
		}catch (Exception e){}
		return matrix;
	}
	
	public Window[] getWindow()
	{
		Window v[] = new Window[this.nTransitions];
		try(FileInputStream file = new FileInputStream(new File(this.fileName)))
		{	
			// Create HSSFWorkbook object to handle Excel XLS
			HSSFWorkbook workbook = new HSSFWorkbook(file);
			
			// Get the first sheet
			HSSFSheet sheet = workbook.getSheetAt(4);	// The fourth sheet of the file contains the window of transitions
			
			// Iterate row by row and cell by cell
			Iterator<Row> rowIt = sheet.iterator();
			Row row;
			while(rowIt.hasNext())
			{
				int j = 0;
				row = rowIt.next();
				Iterator<Cell> cellIterator = row.cellIterator();
				Cell cell;
				while(cellIterator.hasNext())
				{
					cell = cellIterator.next();
					if(row.getRowNum() == 0)
					{
						v[j] = new Window();
						v[j].setAlpha((long)cell.getNumericCellValue());
					}
					if(row.getRowNum() == 1)
					{
						v[j].setBeta((long)cell.getNumericCellValue());
					}
					
					if(j < this.nTransitions)
						j++;
				}
			}
			workbook.close();
			file.close();
		}catch (Exception e){}
		return v;
	}
	
	
	public int getPlaces()
	{
		return this.nPlaces;
	}
	
	public int getTransitions()
	{
		return this.nTransitions;
	}	
}