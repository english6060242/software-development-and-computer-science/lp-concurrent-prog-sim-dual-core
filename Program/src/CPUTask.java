package tp3;

public class CPUTask extends Thread 
{
	private int id;
	private Monitor m;
	private PetriNet Rdp;
	
	public CPUTask(int id, Monitor m, PetriNet Rdp)
	{
		this.id = id;
		this.m = m;
		this.Rdp = Rdp;
	}
	
	public void run()
	{
		while(!this.Rdp.getTerminated())
		{
			if(!this.Rdp.getTerminated())
			{
				if(this.id == 0)
				{
					try 
		        	{
						this.m.enterAndFire(2);
						this.m.enterAndFire(3);
		            } 
		        	catch (InterruptedException e) 
		        	{
		        		System.out.printf("GENERATOR FAILURE\n");
		                return;
		        	}
				}
				if(this.id == 1)
				{
					try 
		        	{
						this.m.enterAndFire(9);
						this.m.enterAndFire(10);	
		            } 
		        	catch (InterruptedException e) 
		        	{
		        		System.out.printf("GENERATOR FAILURE\n");
		                return;
		        	}
				}
			}
		}
	}
}