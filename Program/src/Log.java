package tp3;

import java.io.PrintWriter;

public class Log extends Thread{

	 private int count = 0;

	 private PrintWriter pw; // will make the general log
	 private PrintWriter pw2; // will make the log for T-Invariants

	 private int sleepTime;
	 private PetriNet Rdp;


	 public Log(PrintWriter pw, PrintWriter pw2, PetriNet Rdp, int i)
	 {
		 this.pw = pw;
		 this.pw2 = pw2;
		 this.sleepTime = i;
		 this.Rdp = Rdp;
	 }
	 
	 public void run() 
	 {
		 while (!(isInterrupted())) 
		 {
			 pw.printf("Log %d: \n" , count);
			 pw.printf("Tasks in buffer 1: %d \n", Rdp.getMarking(2));
			 pw.printf("CPUPower1 state: %s - %s \n", Rdp.getCPUStateArray()[0].getON_OFF_State(), Rdp.getCPUStateArray()[0].getActivityState());
			 pw.printf("Tasks in buffer 2: %d \n", Rdp.getMarking(9));
			 pw.printf("CPUPower2 state: %s - %s \n", Rdp.getCPUStateArray()[1].getON_OFF_State(), Rdp.getCPUStateArray()[1].getActivityState());
			 pw.printf("Completed tasks: CPU1: %d , CPU2: %d\n", Rdp.getProcessedB1() , Rdp.getProcessedB2());
			 try 
			 {
				 sleep(sleepTime);
			 } 
			 catch (InterruptedException ex) 
			 {
				 if(ex.getMessage().equals("sleep interrupted"))
				 {
					 return;
				 }
			 }
			 count++;
		 }
	 }
	 
	 public void end(float time)
	 {
		 interrupt();
		 pw.printf("\n\n\nArrival Rate: %d - Service Rate CPU1: %d - Service Rate CPU2: %d \n" , Rdp.getVentana(0).getAlpha() , Rdp.getVentana(3).getAlpha() , Rdp.getVentana(10).getAlpha()); 
		 pw.printf("Generated elements: %d\n"  , Rdp.getIncomingB1() + Rdp.getIncomingB2());
		 pw.printf("BufferCPU1: %d  - BufferCPU2: %d\n"  , Rdp.getIncomingB1() , Rdp.getIncomingB2());
		 pw.printf("Processed elements: Cpu1: %d , Cpu2: %d\n", Rdp.getProcessedB1() , Rdp.getProcessedB2());
		 pw.printf("Final execution time: %f: \n" , time);
		 pw.printf("P-Invariant Test result: %s \n" , Boolean.toString(Rdp.isPinvariant()));
		 if(!Rdp.isPinvariant())
		 {
			 pw.printf("P-Invariant Log: %s \n" , Rdp.getPinvariantLog()); 
		 }
		 pw2.print(Rdp.getTinvariantLog());
	 }
}
