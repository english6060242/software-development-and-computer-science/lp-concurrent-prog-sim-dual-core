package tp3;

public class TaskGenerator extends Thread {
    private Monitor m;
    private int count = 0;
    private int MAX;

    public TaskGenerator(Monitor m, int max) 
    {
        super();
        this.m = m;
        this.MAX = max;
    }

    public void run() 
    {
        while (count < MAX) 
        {
            try 							
            {
                m.entroydisparo(0);			
            } 								
            catch (InterruptedException e) 
            {
                System.out.printf("GENERATOR FAILURE\n");
                return;
            }
            count++;
        }
        System.out.printf("GENERATOR: I finished generating %d data\n", count);
    }
}
