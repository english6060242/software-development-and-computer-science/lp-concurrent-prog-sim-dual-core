package tp3;

public class ArrivalData extends Thread 
{
	private int id;
	private PetriNet Rdp;
	private Monitor m;
	
	public ArrivalData(int id, PetriNet Rdp, Monitor m)
	{
		this.id = id;
		this.Rdp = Rdp;
		this.m = m;
	}
	
	public void run()
	{
		while(!this.Rdp.getTerminado())
		{
			if(!this.Rdp.getTerminado())
			{
				if(this.id == 0)
				{
					try 
		        	{
		        		this.m.enterAndFire(1);	
		            } 
		        	catch (InterruptedException e) 
		        	{
		        		System.out.printf("GENERATOR FAILURE\n");
		                return;
		        	}
				}
				if(this.id == 1)
				{
					try 
		        	{
		        		this.m.enterAndFire(8);	
		            } 
		        	catch (InterruptedException e) 
		        	{
		        		System.out.printf("GENERATOR FAILURE\n");
		                return;
		        	}
				}
			}
		}
	}
}
