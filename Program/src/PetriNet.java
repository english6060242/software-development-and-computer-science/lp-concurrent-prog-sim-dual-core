package tp3;

public class PetriNet 
{
    private ExcelFiles f;
	private int[] marking;
    private int[][] incidenceMatrixPositive;         
    private int[][] incidenceMatrixNegative;
    private int[][] inhibitionMatrix;
    private boolean[] sensitizedTransitions;
    
    private int places, transitions;
    private int processedB1, processedB2;
    private int MAX;
    private CPUState[] cpuStates;
    
    private Ventana[] windows;
    private long[] timeStamps;
    
    private String pinvariantLog = "";
    private boolean pinvariant ;

    private String tinvariantLog = "";
    
    public PetriNet(ExcelFiles f, int m)
    {
    	this.f = f;
    	this.places = f.getPlaces();
    	this.transitions = f.getTransitions();
    	this.incidenceMatrixPositive = f.getMatrix(0);
    	this.incidenceMatrixNegative = f.getMatrix(1);
    	this.inhibitionMatrix = f.getMatrix(3);
    	this.marking = f.getInitialMarking();
    	this.sensitizedTransitions = new boolean[this.transitions];
    	this.processedB1 = 0;
    	this.processedB2 = 0;
    	this.MAX = m;
        cpuStates = new CPUState[2];
        cpuStates[0] = new CPUState();
        cpuStates[1] = new CPUState();
        this.timeStamps = new long[transitions];
        this.windows = f.getWindows();
    	sensitize();
    	// Uncomment to visualize data obtained from the Excel file
    	//this.printMarking();
        //this.printSensitized();
        //this.printPositiveIncidenceMatrix();
        //this.printNegativeIncidenceMatrix();
    	//this.printWindows();
    }
    
    // To sensitize transitions, iterate over the current marking and the negative incidence matrix
    // following the logical guideline of the fundamental equation of Petri nets.
    public void sensitize()
    {
    	for(int i = 0 ; i < this.transitions ; i++)
    	{
    		for(int j = 0; j <this.places ; j++)
    		{
    			if (!(marking[j] >= incidenceMatrixPositive[j][i])) 
    			{  
    				sensitizedTransitions[i] = false;
    				break;
    			}
    			sensitizedTransitions[i] = true;
    			if(inhibitionMatrix[j][i] == 1)
    			{
    				if(marking[j] != 0)
        			{
        				sensitizedTransitions[i] = false;
        				break;
        			}
    			}
             }
         }
    }

    // Firing a transition corresponds to modifying the current marking
    // based on the combined incidence matrix Ic = (I+) + (I-).
    public void fire(int transition)
    {
    	boolean[] enabledBefore = new boolean[this.transitions];
        boolean[] enabledAfter = new boolean[this.transitions];
        
    	for(int i = 0; i < this.transitions ; i++)
    	{
    		if(this.isTimed(i))
    		{
    			enabledBefore[i] = this.isSensitized(i);
    		}
    		else
    		{
    			enabledBefore[i] = true;
    		}
    	}
    	
    	for(int j = 0 ; j < this.places ; j++)
    	{
    		marking[j] += incidenceMatrixNegative[j][transition];
        }
        
        for(int j = 0 ; j < this.places ; j++)
        {
            marking[j] -= incidenceMatrixPositive[j][transition];
        }
        
        sensitize();
        
        for(int i = 0; i < this.transitions ; i++)
    	{
    		if(this.isTimed(i))
    		{
    			enabledAfter[i] = this.isSensitized(i);
    		}
    		else
    		{
    			enabledAfter[i] = false;
    		}
    		
    		if(!enabledBefore[i] && enabledAfter[i])  // If transitions that were not sensitized are now sensitized after firing
    		{
    			this.timeStamps[i] = System.currentTimeMillis();	// Start counting time to be able to fire them
    		}
    	}
        
        countTasks(transition);	
        pinvariantTest(transition);
        addTinvariantLog(transition); 
        updateState(transition);  // Mechanism to end execution with the initial marking.
    }
    
    public long getTimeStamp(int t)
    {
    	return this.timeStamps[t];
    }
    
    public boolean isTimed(int t)
    {
    	return this.windows[t].getAlpha() != 0;
    }
    
    public Ventana getWindow(int t)
    {
    	return this.windows[t];
    }

    public void countTasks(int t)
    {
    	if(t == 1)
        {
        	this.IngresadasB1++;
        }
        if(t == 8)
        {
        	this.IngresadasB2++;
        }
    	if(t == 3)
        {
        	this.ProcesadasB1++;
        }
        if(t == 10)
        {
        	this.ProcesadasB2++;
        }
    }
    
    public int getIngresadasB1 ()
    {
    	return this.IngresadasB1;
    }
    public int getIngresadasB2 ()
    {
    	return this.IngresadasB2;
    }
    public int getProcesadasB1 ()
    {
    	return this.ProcesadasB1;
    }
    public int getProcesadasB2 ()
    {
    	return this.ProcesadasB2;
    }
    
    public void updateState(int t)
    {
    	if(t == 5 || t == 6)
    	{
    		cpuStates[0].setON_OFF_State("ON");
    	}
    	if(t == 2)
    	{
    		cpuStates[0].setActivityState("ACTIVE");
    	}
    	if(t == 3)
    	{
    		cpuStates[0].setActivityState("IDLE");
    	}
    	if(t == 4)
    	{
    		cpuStates[0].setON_OFF_State("STAND_BY");
    	}
    	if(t == 12 || t == 13)
    	{
    		cpuStates[1].setON_OFF_State("ON");
    	}
    	if(t == 9)
    	{
    		cpuStates[1].setActivityState("ACTIVE");
    	}
    	if(t == 10)
    	{
    		cpuStates[1].setActivityState("IDLE");
    	}
    	if(t == 11)
    	{
    		cpuStates[1].setON_OFF_State("STAND_BY");
    	}
    }
    
    public CPUState[] getCPUStateArray()
    {
    	return this.cpuStates;
    }
    
    public boolean isSensitized(int i)
    {
    	return this.sensitizedTransitions[i];
    }
    
    public int getMarking(int Place)
    {
    	return this.marking[Place];
    }
    
    public boolean isTerminated()
    {
    	if(((this.processedB1 + this.processedB2) == this.MAX) && ((cpuStates[0].getON_OFF_State().equals("STAND_BY")) && (cpuStates[1].getON_OFF_State().equals("STAND_BY"))))
    	{
    		return true;
    	}
    	return false;
    }
    
    // For analyzing invariants
    
    public void pinvariantTest(int t)
    {
        if((marking[0] + marking[1]) != 1)
        {
        	pinvariant = false;
        	pinvariantLog = pinvariantLog + "Failed P-Invariant: M(P0) + M(P1) = 1\n After firing T" + t + "\n";
        }
        if((marking[3] + marking[4]) != 1)
        {
        	pinvariant = false;
        	pinvariantLog = pinvariantLog + "Failed P-Invariant: M(P3) + M(P4) = 1\n After firing T" + t + "\n";
        }
        if((marking[5] + marking[6] + marking[8]) != 1)
        {
        	pinvariant = false;
        	pinvariantLog = pinvariantLog + "Failed P-Invariant: M(P5) + M(P6)+ M(P8) = 1\n After firing T" + t + "\n";
        }
        if((marking[10] + marking[11]) != 1)
        {
        	pinvariant = false;
        	pinvariantLog = pinvariantLog + "Failed P-Invariant: M(P10) + M(P11) = 1\n After firing T" + t + "\n";
        }
        if((marking[12] + marking[13] + marking[15]) != 1)
        {
        	pinvariant = false;
        	pinvariantLog = pinvariantLog + "Failed P-Invariant: M(P12) + M(P13)+ M(P14) = 1\n After firing T" + t + "\n";
        }
        pinvariant = true;
    }
    
    public boolean isPinvariant() {return pinvariant;}

    public String getPinvariantLog(){return pinvariantLog;}
    
    public void addTinvariantLog(int trans) {tinvariantLog = tinvariantLog +"T" + trans + "-";}
    
    public String getTinvariantLog(){return tinvariantLog;}
    
    // Used for Debugging
    
    public void printSensitized()
    {
    	for(int i =0; i < this.sensitizedTransitions.length; i++)
    	{
    		if(this.sensitizedTransitions[i])
    		{
    			System.out.println("Transition "+i+" sensitized");
    		}
    		else
    		{
    			System.out.println("Transition "+i+" not sensitized");
    		}
    	}
    }
    
    public void printPositiveIncidenceMatrix()
    {
    	for(int i =0; i < this.sensitizedTransitions.length; i++)
    	{
    		for(int j =0; j < this.sensitizedTransitions.length; j++)
    		{
    			System.out.printf("M ["+i+"]["+j+"] = "+this.incidenceMatrixPositive[i][j]);
    		}
    		System.out.println();
    	}
    	System.out.println();
    }
    
    public void printNegativeIncidenceMatrix()
    {
    	for(int i =0; i < this.places; i++)
    	{
    		for(int j =0; j < this.transitions; j++)
    		{
    			System.out.printf("M ["+i+"]["+j+"] = "+this.incidenceMatrixNegative[i][j]);
    		}
    		System.out.println();
    	}
    	System.out.println();
    }
    
    public void printMarking() 
    {
    	System.out.printf("Current Marking: ");
    	for (int i = 0; i < marking.length; i++) 
        {
            System.out.printf("%d, ", marking[i]);
        }
        System.out.println();
    }
    
    public void printWindows()
	{
		for(int i = 0; i < this.transitions; i++)
		{
			System.out.println("T"+i+" Alpha: "+this.windows[i].getAlpha()+" Beta: "+this.windows[i].getBeta());
		}
	}
    
    // Getter never used, exists only to get rid of the warning
    public ExcelFiles getExcelFiles() {return this.f;}
}
